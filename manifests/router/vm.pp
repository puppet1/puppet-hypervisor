#
# classes for creating virtual machines on the hypervisor server
#

class hypervisor::router::vm ()
{

  include hypervisor::vm

  lvm::logical_volume { 'router-disk':
    volume_group      => $hypervisor::vm::root_vg,
    fs_type           => 'ext4',
    size              => '2G',
    mounted           => false,
    mountpath_require => false,
  }

  lvm::logical_volume { 'router-swap':
    volume_group => $hypervisor::vm::swap_vg,
    createfs     => false,
    size         => '1G',
  }

  $networks = hiera('networks')
  #$int_gw = $networks['internal']['gateway']
  $int_ip = $networks['internal']['gateway']
  $int_netmask = $networks['internal']['netmask']
  $int_dns = $networks['internal']['dns']

  $ext_ip = $networks['external']['gateway']
  $ext_netmask = $networks['external']['netmask']
  $ext_dns = $networks['external']['dns']



  Exec { "/usr/local/bin/mkrootdisk.sh /dev/${hypervisor::vm::root_vg}/router-disk":
    require     => [ File['/usr/local/bin/mkrootdisk.sh'], File['/usr/local/bin/inodecount.sh'], Lvm::Logical_volume['router-disk'] ],
    onlyif      => "/usr/bin/test `/usr/local/bin/inodecount.sh /dev/${hypervisor::vm::root_vg}/router-disk` = 11",
    environment => ["INTIP=${int_ip}",
                    "INTNM=${int_netmask}",
                    "EXTIP=${ext_ip}",
                    'EXTGW=1.2.3.4',
                    "EXTNM=${ext_netmask}",
                    "DNS=${int_dns}",
                    'VMNAME=router',
                    "AUTHKEY=${hypervisor::vm::authorized_key}"],
    timeout     => 0,
  }

# VM definition



  libvirt::domain { 'router':
    devices_profile => 'headless',
    dom_profile     => 'myprofile',

    uuid            => hypervisor_generate_uuid($name),
    disks           => [{'type' => 'block',
                    'device'    => 'disk',
                    'source'    => {'dev' => "/dev/${hypervisor::vm::root_vg}/router-disk"},
                    'bus'       => 'virtio',
                    'driver'    => {'name'   => 'qemu',
                                  'type'  => 'raw',
                                  'cache' => 'directsync',
                                },
                    },
                    {'type'  => 'block',
                    'device' => 'disk',
                    'source' => {'dev' => "/dev/${hypervisor::vm::swap_vg}/router-swap"},
                    'bus'    => 'virtio',
                    'driver' => {'name'   => 'qemu',
                                  'type'  => 'raw',
                                  'cache' => 'directsync',
                                },
                    },],
    interfaces      => [ {
                    'network' => 'internal',
                    'mac'     => '52:54:00:ac:ab:01',
                    'type'    => 'virtio',
                    'ip'      => $int_ip,
                    'name'    => 'router-int'
                    },
                    {
                    'network' => 'external',
                    'mac'     => '52:54:00:ac:ab:03',
                    'type'    => 'virtio',
                    'ip'      => $ext_ip,
                    'name'    => 'router-ext'
                    },
                    ],
    autostart       => true,
    require         => Exec["/usr/local/bin/mkrootdisk.sh /dev/${hypervisor::vm::root_vg}/router-disk"],
  }

}
