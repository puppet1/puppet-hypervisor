#
# xen
#

class hypervisor::xen (
) {


  case "${operatingsystem}" {
  'Debian': { 
    $packages = [
      'xen-hypervisor',
    ]

  } 
  'FreeBSD':  { 
    $packages = [
      'lsblk',
      'xen-kernel',
      'e2fsprogs',
      'fusefs-ext2',
      'emulators/linux_base-c7',
      #'strace',
      'gdb',
      'xen-tools', # doesn't work on hardenedbsd
      'libvirt'
    ]

    Exec {
      user    => 'root',
      cwd     => '/root/',
      timeout => 9999,
      path    => '/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin',
      environment => ['HOME=/root' ]
    }
    
    exec { "load linux64":
      command => "kldload linux64 || true",
    }



  }
  default:  { 
     fail("os ${operatingsystem} not supported")

    } 
  }
 
  ensure_packages($packages)






}
