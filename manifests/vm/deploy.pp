#
# resource type for deploying virtual machines 
#

define hypervisor::vm::deploy (
  $ensure                                                    = 'present',
  String $domain                                             =  lookup('domain'),
  Stdlib::Fqdn $public_hostname                              = "${name}.${domain}",
  String $devices_profile                                     = 'headless',
  String $dom_profile                                         = 'headless',
  String $rootsize                                           = '4G',
  String $swapsize                                           = '1G',
  Boolean $datadisk                                          = false,
  String $datasize                                           = '4G',
  Boolean $autostart                                         = true,
# this is in hiera in the profiles
#  String $kernel                                             = $hypervisor::vm::kernel,
#  String $initrd                                             = $hypervisor::vm::initrd,
#  Integer $memory                                            = 1000,
#  Integer $cpus                                              = 1,
  String  $os_version                                    = 'buster',
  Optional[ Stdlib::IP::Address::V4 ] $dns                   = '0.0.0.0',
  Array $net_interfaces             = [],
  $type = 'lala',

) {

  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }


  #include hypervisor::router::vm
  include hypervisor::vm

  if "$hypervisor::vm::type" == "xen" {
    $interface_type = "netfront"
  } else {
    $interface_type = "virtio"
  }

# Storage
  if "${operatingsystem}" == "Debian" {

    $block_prefix = "/dev"

  #FIXME: disks should not get added to fstab
    lvm::logical_volume { "${name}-disk":
      ensure            => $ensure,
      fs_type           => 'ext4',
      size              => $rootsize,
      atboot            => false,
      mounted           => false,
      mountpath_require => false,
      volume_group      => $hypervisor::vm::root_vg,
    }

    lvm::logical_volume { "${name}-swap":
      ensure       => $ensure,
      createfs     => false,
      size         => $swapsize,
      volume_group => $hypervisor::vm::swap_vg,

    }

    if $datadisk {
      lvm::logical_volume { "${name}-data":
        ensure            => $ensure,
        fs_type           => 'ext4',
        size              => $datasize,
        atboot            => false,
        mounted           => false,
        mountpath_require => false,
        volume_group      => $hypervisor::vm::data_vg,

      }
    }

  } else {
    
    $block_prefix = "/dev/zvol"



    zfs { "${hypervisor::vm::root_vg}/${name}-disk":
      ensure => $ensure,
      volsize => $rootsize,
    }
    exec { "create fs ${name}-disk":
      command     => "mke2fs -t ext4 ${block_prefix}/${hypervisor::vm::root_vg}/${name}-disk",
      subscribe   => Zfs["${hypervisor::vm::root_vg}/${name}-disk"],
      refreshonly =>  true
    }
    zfs { "${hypervisor::vm::root_vg}/${name}-swap":
      ensure => $ensure,
      volsize => $swapsize,
    }
    if $datadisk {
      zfs { "${hypervisor::vm::root_vg}/${name}-data":
            ensure => $ensure,
            volsize => $datasize,
          }
      exec { "create fs ${name}-data":
        command     => "mke2fs -t ext4 ${block_prefix}/${hypervisor::vm::root_vg}/${name}-data",
        subscribe   => Zfs["${hypervisor::vm::root_vg}/${name}-disk"],
        refreshonly =>  true
      }


    }



    }



# Debootstrap

  #FIXME: mkrootdisk does not get run again when it fails
  # to have puppet rerun this script, make a new filesystem on the disk (deleting files does not work)
  Exec { "/usr/local/bin/mkrootdisk.sh ${block_prefix}/${hypervisor::vm::root_vg}/${name}-disk":
    onlyif      => "test `/usr/local/bin/inodecount.sh ${block_prefix}/${hypervisor::vm::root_vg}/${name}-disk` = 11",
    environment => ["INTIP=${internal_ip}",
                    "INTGW=${internal_gw}",
                    "INTNM=${internal_nm}",
                    "EXTIP=${external_ip}",
                    "EXTGW=${external_gw}",
                    "EXTNM=${external_nm}",
                    "DNS=${dns}",
                    "VMNAME=${name}",
                    "AUTHKEY=${hypervisor::vm::authorized_key}",
                    "DOMAIN=${hypervisor::vm::domain}",
                    "INTMETHOD=${internal_method}",
                    "EXTMETHOD=${external_method}",
                    "DEBIAN_VERSION=${os_version}",

                    ],
    logoutput   => true,
    timeout     => 0,
    require     => [ 
      Class['hypervisor::vm'], 
      #FIXME: Lvm::Logical_volume["${name}-disk"]
    ],
  }

# Firewalling
#
# FIXME (on bsd) virnwfilterdefinexml not supported by connection driver
  if "${operatingsystem}" == "Debian" {

    libvirt::nwfilter { "${name}-internal":
      #ensure            => $ensure,
      ip                => $internal_ip,
      publictcpservices => $internal_tcp_services,
      publicudpservices => $internal_udp_services,
      customtcprules    => $internal_custom_tcp_rules,
      customudprules    => $internal_custom_udp_rules,
      uuid              => hypervisor_generate_uuid("${name}internal"),

    }

    libvirt::nwfilter { "${name}-external":
      #ensure            => $ensure,
      ip                => $external_ip,
      publictcpservices => $external_tcp_services,
      publicudpservices => $external_udp_services,
      customtcprules    => $external_custom_tcp_rules,
      customudprules    => $external_custom_udp_rules,
      uuid              => hypervisor_generate_uuid("${name}external"),

    }
  }
# VM definition

  exec { "delete old ${name} config":
    command => "/bin/rm /etc/libvirt/qemu/${name}.xml || true",
  }

  exec { "restart_${name}":
    command     => "/usr/bin/virsh shutdown ${name} ; /usr/bin/virsh define /etc/libvirt/qemu/${name}.xml ; /usr/bin/virsh start ${ name } ",
    subscribe   => [
      File["/etc/libvirt/qemu/${name}.xml"],
    ],
    require     => [
      Libvirt::Domain[$name],
    ],

    refreshonly => true,
  }

  file { "/etc/libvirt/qemu/${name}.xml":
    audit  => 'content',
    notify => Exec["restart_${name}"],
  }
   notify { "prof is: $devices_profile": }

  if "${devices_profile}" == '9p' {
    
    notify { "we are using 9p: $devices_profile": }
    file { "/srv/rootfs/${name}":
      ensure => directory
    }
    mount { "/srv/rootfs/${name}":
      ensure => 'mounted',
      device => "${block_prefix}/${hypervisor::vm::root_vg}/${name}-disk",
      fstype => "ext4"
    }
    # eventually security model should be mapped (so qemu doesn have to run as root): https://lists.gnu.org/archive/html/qemu-devel/2010-05/msg02673.html
    # issue is how to create the rootfs when mapped is used (cause ownership info will be stored in extended attributes on host)
    # squash should not be used, cause it will trow away information
    $devices = { "filesystem" => { "attrs" => { 
                                                "accessmode" => "passthrough",
                                                #"fmode"      => "600",
                                                #"dmode"      => "700"
                                              },
                                   "values" => { 
                                                 "source" => { "attrs" => { "dir" => "/srv/rootfs/${name}" }},
                                                 "target" => { "attrs" => { "dir" => 'root9p' }}    
                                                }
                                  } 
               }


    $disks = undef

  } else {
    notify { "we are NOT using 9p: $devices_profile": }

    mount { "/srv/rootfs/${name}":
      ensure => 'absent',
    }

    $devices = undef
    $disks =  hypervisor_disks($name,$hypervisor::vm::root_vg,$hypervisor::vm::swap_vg,$hypervisor::vm::data_vg,$datadisk)
  }




libvirt::domain { $name:
  type            => $hypervisor::vm::type,
  devices_profile => $devices_profile,
  dom_profile     => $dom_profile,
  devices         => $devices,
  disks           => $disks,
  #boot           => 'hd',
  interfaces      => hypervisor_interfaces($net_interfaces),
  autostart       => $autostart,
    require         => [ 
                        Exec["/usr/local/bin/mkrootdisk.sh ${block_prefix}/${hypervisor::vm::root_vg}/${name}-disk"],
                        Exec["delete old ${name} config"],
                        #Hypervisor::network["${internal_net}"],
                        #Hypervisor::network["${external_net}"],
                        #FIXME Libvirt::Nwfilter["${name}-internal"],
                        #FIXME Libvirt::Nwfilter["${name}-external"] 
                       ],
    uuid            => gen_consistent_uuid($name),


  }

}
