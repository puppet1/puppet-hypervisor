#
# creates bridges for libvirt
#

class hypervisor::networks (
  String $domain         =  lookup('domain', undef, undef, 'no_domain_set'),
) {


  require put::base


 


  if "${operatingsystem}" == "Debian" {

  require put::buster
  include network

  #packages
    $packages = [
      'bridge-utils',
      'ifupdown',
      'ebtables',

    ]

    ensure_packages($packages)

   sysctl { 'net.ipv4.ip_forward': value => '1' }



# create networks's defined in hiera
    $networks = hiera_hash('networks')
    create_resources ( hypervisor::network , $networks )

  }
}


