define hypervisor::network (
  $ensure       = 'present'   ,
  # allocation network is to make host addresses readable (when your network uses a netmask that are not /24 )
  Stdlib::IP::Address $allocation_network,

  Stdlib::IP::Address $netmask,
  Stdlib::IP::Address $network,
  Stdlib::IP::Address $gateway,
  $dns       = undef,
  Hash $routes           = {},
  String $bridge = $name,
  String $forward_mode = 'bridge',
  # FIXME: config should not be separate for nat 
  Stdlib::IP::Address $nat_ip = '10.13.12.1',
  Stdlib::IP::Address $nat_netmask = '255.255.255.0',
  Stdlib::IP::Address $nat_dhcp_start = '10.13.12.42',
  Stdlib::IP::Address $nat_dhcp_end = '10.13.12.254',
  $nat_forward_dev = lookup(profile::primary_network::interface),

) {


  if $forward_mode == 'nat' {

    libvirt::network { $name:
      forward_mode => $forward_mode,
      forward_dev  => lookup(profile::primary_network::interface),
      bridge       => 'br-nat',
      ip_address   => $nat_ip,
      ip_netmask   => $nat_netmask,
      dhcp_start   => $nat_dhcp_start,
      dhcp_end     => $nat_dhcp_end,
    }


  } else {

    libvirt::network { $name:
      forward_mode => 'bridge',
      bridge       => $bridge,
    }

    if defined(Network::Interface["$bridge"]) {
    #if defined(Mroute[$bridge]) {
    # Error: Evaluation Error: Error while evaluating a Resource Statement, Evaluation Error: A substring operation does not accept a String as a character index. Expected an Integer (file: /etc/puppet/code/modules/hypervisor/manifests/network.pp, line: 44, column: 31) (file: /etc/puppet/code/modules/hypervisor/manifests/networks.pp, line: 37) on node hypr.example.com
   
    #if true {
      #FIXME: how the fuck is this supposed to work??

      alert("Multiple networks on same bridge, make sure that gateway is the same on both of them (p2p routes), and netmask is /32")
      # FIXME: ^ make sure that is the case

    } else {

      # FIXME: configures hypervisor as the gateway for now
      network::interface { $bridge:
        bridge_ports => ['none'],
        bridge_stp   => 'off',
        bridge_fd    => 0,
        address      => $gateway,
        netmask      => $netmask,
      }
     network::mroute { $bridge:
      routes => $routes,
     }


    }

    
  }






}


