class hypervisor (
  String $type = 'kvm',
  String $nat_netmask,
  String $nat_dhcp_start,
  String $nat_dhcp_end,
) {

# Make sure we have libvirt installed and running

  class {'libvirt': }


# Install packages to make sysadmin life easy on hypervisors

  $packages = [
    'bridge-utils',
    'build-essential',
    'debootstrap',
    'devscripts',
    'fakeroot',
    'ipmitool',
    'linux-source',
    'lm-sensors',
    'smartmontools',
  ]
   
  ensure_packages($packages)


}

