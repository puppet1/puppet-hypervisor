#
# creates libvirt vm
#

class hypervisor::vm (
  String $type = 'xen',
  String $root_vg,
  String $swap_vg,
  String $data_vg,
  # defined in libvirt profile
  # String $kernel         = '/etc/xen/boot/stable.vmlinuz',
  # String $initrd  = undef,
  String $authorized_key = 'none',
  String $domain         =  lookup('domain', undef, undef, 'no_domain_set'),

) {

include git

#packages

if "${type}" == "xen" {
  require hypervisor::xen
}

if "${operatingsystem}" == "Debian" {
  $rootgroup = 'root'
  $packages = [
    'debootstrap',
    'linux-image-4.19.0-9-cloud-amd64',
  ]
} else {
  $rootgroup = 'wheel'
  $packages = [
    'debootstrap',
  ]

}

  ensure_packages($packages)
# scripts

  file { '/usr/local/etc/debian-archive-keyring.gpg':
    ensure => present,
    owner  => root,
    group  => $rootgroup,
    mode   => '0700',
    source => "puppet:///modules/${module_name}/debian-archive-keyring.gpg",
  }


  file { '/usr/local/bin/mkrootdisk.sh':
    ensure => present,
    owner  => root,
    group  => $rootgroup,
    mode   => '0700',
    source => "puppet:///modules/${module_name}/scripts/mkrootdisk.sh",
  }

  file { '/usr/local/bin/inodecount.sh':
    ensure => present,
    owner  => root,
    group  => $rootgroup,
    mode   => '0700',
    source => "puppet:///modules/${module_name}/scripts/inodecount.sh",
  }


}

