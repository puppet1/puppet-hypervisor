#
# a classes for virtual machines r
#

class hypervisor::vms (
  String $domain         =  lookup('domain', undef, undef, 'no_domain_set'),
  $vm_defaults = { 
    devices_profile   => '9p',
    dom_profile       => '9p',
  }


) {
  require put::admins

  require put::base

 

  if "${operatingsystem}" == "Debian" {
  
    require buildkernel::kernelprep
    buildkernel::kernel { 'test1':
    }


    buildkernel::kernel { 'test2':
      kernel_version => '4.19.0-9-cloud-amd64',
      kernel_type    => 'debian',
    }

    buildkernel::kernel { 'p9':
      kernel_version => '4.19.0-13-amd64',
      kernel_type    => 'debian',
    }


  }

   file { "/srv/":
    ensure => directory
  }
 

  file { "/srv/rootfs/":
    ensure => directory
  }
    # create vm's defined in hiera
  $vms = hiera_hash('vms')

   $vms.each |String $resource, Hash $attributes| {
    Resource["hypervisor::vm::deploy"] {
      $resource: * => $attributes;
      default:   * => $vm_defaults;
    }
  }

  #create_resources ( hypervisor::vm::deploy , $vms )

}

