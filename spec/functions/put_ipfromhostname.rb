
require 'spec_helper'

describe 'put_ipfromhostname' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      context 'with parameters' do
        it { 
          is_expected.to run.with_params('proksie', 'net-ext').and_return('94.142.245.192') 
          is_expected.to run.with_params('puppetmaster', 'net-int').and_return('192.168.1.200') 
          is_expected.to run.with_params('test', 'net-int').and_return('192.168.1.150') 
          is_expected.to run.with_params('test', 'net-nat').and_raise_error(Puppet::Error, /interface needs ip or host/)
          is_expected.to run.with_params('test2', 'net-nat').and_return(nil)
          is_expected.to run.with_params('test2', 'net-int').and_raise_error(Puppet::Error, /multiple interfaces in same network, not sure what ip to give you/)


        }

      end
    end
  end
end

