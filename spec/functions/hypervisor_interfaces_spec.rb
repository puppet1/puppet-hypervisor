
require 'spec_helper'

describe 'gen_interfaces' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      in_if = [
        {
          "ip" => "192.168.1.2",
          "network" => "internal"
        },
        {
          "ip" => "10.1.2.3",
          "network" => "internal",
          "mac" => "52:54:00:11:22:33"

        },
        {
          "host" => 12,
          "network" => "internal",
          "mac" => "52:54:00:11:22:33"
        }

      ]
      out_if = [
        {
          "ip" => "192.168.1.2",
          "network" => "internal",
          "type" => "virtio",
          "mac" => "52:54:00:c0:ca:1e"
        },
        {
          "ip" => "10.1.2.3",
          "network" => "internal",
          "type" => "virtio",
          "mac" => "52:54:00:11:22:33"

        },
        {
          "ip" => "192.168.23.12",
          "network" => "internal",
          "type" => "virtio",
          "mac" => "52:54:00:11:22:33"

        }

      ]


      context 'with parameters' do
        it { 
          is_expected.to run.with_params(in_if).and_return(out_if) }
      end
    end
  end
end
