require 'yaml'

  #gives full ip from host id,  by using allocation_subnet
  def ipfromhostid(network, host)
    networks = call_function('lookup', "networks")

    gateway = IPAddr.new networks[network]['gateway']
    netmask = IPAddr.new networks[network]['netmask']

    if ! (networks[network].key?('allocation_network') or networks[network].key?('network')) then
      raise Puppet::Error.new "no network info in network definition, can't find ip like that"
    end


    # a p2p network, has an allocation subnet
    if networks[network].key?('allocation_network') then
      net = IPAddr.new networks[network]['allocation_network']
      if networks[network]['netmask'] != '255.255.255.255' then
        raise Puppet::Error.new "allocation_network is for p2p networks, and your netmask != 32"
      end
    else
      net = IPAddr.new networks[network]['network']
    end

    net.to_range.first(host + 1).last().to_s()
  end

  # get ip from interface definition
  def ipfrominterface(interface)

    if ! ( interface.key?('ip') or interface.key?('host') ) then
      raise Puppet::Error.new "interface needs ip or host"
    end


    if interface.key?('ip') then
      return interface['ip']
    else
      return ipfromhostid(interface['network'], interface['host'])
    end
  end

  # gives full ip from hostname + network
  def ipfromhostname(hostname, network)
    vms = call_function('lookup', "vms")
    admins = call_function('lookup', "admins")
    networks = call_function('lookup', "networks")

    interfaces = vms[hostname]['net_interfaces']
    count = 0

    for interface in interfaces do
      if interface['network'] == network then
        count = count + 1
        ip = ipfrominterface(interface)
      end
    end

    if count > 1 then
      raise Puppet::Error.new "multiple interfaces in same network, not sure what ip to give you"
    end

    return ip
  end

  def fqdnfromhostname(hostname)
    domain = call_function('lookup', "domain")
    if hostname.include? '.' then
      hostname
    else
      hostname + '.' + domain
    end 
  end

  def loaddata()
    data = {}
    data['domain'] = call_function('lookup', "domain")
    data['vms'] = applytemplatesondict(call_function('lookup', "vms"))
    data['admins'] = applytemplatesondict(call_function('lookup', "admins"))
    data['networks'] = applytemplatesondict(call_function('lookup', "networks"))
    puts data.to_yaml
    #puts data.to_json
    puts JSON.pretty_generate(data)


    return data
  end


  def applytemplatesondict(dict)
    # works on dict of dicts
    # if a dict in the dict has a field template, then it will be merged with another dict in that dict
    
    puts dict.class
    out = {}

    for key, value in dict do
      if value.key?('istemplate') then
        next
      end

      if value.key?('template') then
        tp = dict[value['template']]
        if tp.key?('template') then
          raise Puppet::Error.new "only 1 level of templates supported"
        end
        #{ "23" => [0,3] }.merge({ "23" => [2,3] }) do |key, oldval, newval|
        
        merged = tp.merge(value).reject { |k, v| ["template", "istemplate"].include? k }

        #merged = tp.merge(value) do |key, oldval, newval| 
        #  oldval | newval
        #end
          #.reject { |k, v| ["template", "istemplate"].include? k }
        out[key] = merged 
      else
        # no template used
        out[key] = value
      end
    end

    return out

  end
  def getservicesforinterface(hostname, network)
    FALSE
  end

  def getportsforinterface(hostname, network)
    FALSE
  end

  def getnetgroupsforinterface(hostname, network)
    FALSE
  end


