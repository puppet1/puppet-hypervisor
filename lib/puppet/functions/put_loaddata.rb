require_relative './putcommon.rb'

Puppet::Functions.create_function(:'put_loaddata') do
  def put_loaddata(*args)
    loaddata(*args)
  end
end

