require 'hiera'
require 'hiera/util'
require 'optparse'
require 'pp'
require 'ipaddr'
require_relative './putcommon.rb'
# generates the network interfaces (using info in hiera) for cirrax/libvirt
Puppet::Functions.create_function(:'gen_interfaces') do

  def gen_interfaces(*args)


    in_if = args[0]
    out_if = Array.new
    type = 'virtio'

    vms = call_function('lookup', "vms")
    admins = call_function('lookup', "admins")
    networks = call_function('lookup', "networks")
    
    puts networks


    in_if.each do |intf|
      out = {}
      if not intf['ip'] then
        # calculate ip from host
        out['ip'] = ipfromhostid(intf['network'], intf['host'])
      else
        out['ip'] = intf['ip']
      end
      if not intf['mac'] then
        # gen mac
        out['mac'] = call_function('libvirt_generate_mac',[out['ip']]) 
      else
        out['mac'] = intf['mac']
      end
      out['type'] = type
      out['network'] = intf['network']
      out_if.append(out)
    end

    out_if

  end

end



