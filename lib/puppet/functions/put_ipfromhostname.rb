require_relative './putcommon.rb'

Puppet::Functions.create_function(:'put_ipfromhostname') do
  def put_ipfromhostname(*args)
    ipfromhostname(*args)
  end
end

