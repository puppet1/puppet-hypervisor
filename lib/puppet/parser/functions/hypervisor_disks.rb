module Puppet::Parser::Functions
  newfunction(:hypervisor_disks, :type => :rvalue) do |args|

    name = args[0]
    rootvg = args[1]
    swapvg = args[2]
    datavg = args[3]
    datadisk = args[4]

    disks = Array.new
    disks << {'type'   => 'block',
              'device' => 'disk',
              'source' => {'dev' => "/dev/" + rootvg + "/" + name + "-disk"},
              'bus'    => 'virtio',
              'driver' => {'name'  => 'qemu',
                           'type'  => 'raw',
                           'cache' => 'directsync',
                          },
             }
    disks << {'type'   => 'block',
              'device' => 'disk',
              'source' => {'dev' => "/dev/" + swapvg + "/" + name + "-swap"},
              'bus'    => 'virtio',
              'driver' => {'name'  => 'qemu',
                           'type'  => 'raw',
                           'cache' => 'directsync',
                          },
              }

    if datadisk then
      disks << {'type'   => 'block',
                'device' => 'disk',
                'source' => {'dev' => "/dev/" + datavg + "/" + name + "-data"},
                'bus'    => 'virtio',
                'driver' => {'name'  => 'qemu',
                             'type'  => 'raw',
                             'cache' => 'directsync',
                            },    
                }
    end

    disks

  end
end
