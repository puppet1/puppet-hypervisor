#!/bin/sh

set -eu

DISK=$1

INODECOUNT=$(tune2fs -l ${DISK} |grep 'Inode count' |awk '{print $3}')
INODEFREE=$(tune2fs -l ${DISK} |grep 'Free inodes' |awk '{print $3}')

INODEUSED=$((INODECOUNT - INODEFREE))

echo "${INODEUSED}"

