#!/usr/bin/env bash
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
set -eux

env

TESTING=1

echo $DEBIAN_VERSION

DISK=$1
MOUNTDIR=$(mktemp -d)
ETH1=""
ETH2=""

cleanup(){
  if [ ! $TESTING -eq 1 ]
  then
    if [ ! -z "$MOUNTDIR" ]; then
      #FIXME why is this check necassarry? if mountpoint -q "${MOUNTDIR}"; then
        umount "${MOUNTDIR}"
      #fi
      rm -rf "${MOUNTDIR}"
    fi
  fi
}

mketh0(){
  echo "intmethod is: $INTMETHOD "
  if [[ "$INTMETHOD" != "dhcp" ]]
  then
    if [ ! "$INTIP" = "0.0.0.0" ];then
      if [ "$EXTIP" = "0.0.0.0" ];then
        ETH0="auto eth0\niface eth0 inet static\n  address ${INTIP}\n  netmask ${INTNM}\n  gateway ${INTGW}\n"
      else
        ETH0="auto eth0\niface eth0 inet static\n  address ${INTIP}\n  netmask ${INTNM}\n"
      fi
    elif [ ! "$EXTIP" = "0.0.0.0" ]; then
      ETH0="auto eth0\niface eth0 inet static\n  address ${EXTIP}\n  netmask ${EXTNM}\n  gateway ${EXTGW}\n"
    fi
  else
    ETH0="auto eth0\niface eth0 inet dhcp\n"
  fi
}

mketh1(){
  echo "extmethod is: $EXTMETHOD "
  if [[ "$EXTMETHOD" != "dhcp" ]]
  then
    if [ ! "$INTIP" = "0.0.0.0" ];then
      if [ ! "$EXTIP" = "0.0.0.0" ];then
        ETH1="auto eth1\niface eth1 inet static\n  address ${EXTIP}\n  netmask ${EXTNM}\n  gateway ${EXTGW}\n"
      fi
    fi
  else
    ETH1="auto eth1\niface eth1 inet dhcp\n"
  fi

}


if [ ! -z "$DISK" ]; then

  if [[ `uname` == "FreeBSD" ]]
  then
    if kldload ext2fs 
    then
      echo "loaded ext2"
    fi
    if kldload linux64
    then
      echo "loaded linux64"
    fi
    mount -t ext2fs "$DISK" "$MOUNTDIR"
  else
    mount "$DISK" "$MOUNTDIR"
  fi

  trap cleanup EXIT

  if ls /usr/bin/docker
  then
    echo "docker installed, using that to download the image, cause its much faster then debootstrap"
    CONT=`docker create "debian:$DEBIAN_VERSION"`
    echo "created: $CONT"
    docker export "$CONT" |  tar x -C "$MOUNTDIR"
    docker rm "$CONT"
    
    # FIXME: use our nameserver (once it exists)
    echo "nameserver 1.1.1.1" > "${MOUNTDIR}/etc/resolv.conf"
    chroot ${MOUNTDIR} apt-get -y update
    chroot ${MOUNTDIR} apt-get -y upgrade
    chroot ${MOUNTDIR} apt-get -y install systemd ifupdown netbase puppet git openssh-server curl vim lsb-release python3 udev python
  else
    echo "using debootstrap"

    mirror="http://ftp.nl.debian.org/debian/"
    image="https://cloud.debian.org/images/cloud/buster/daily/20190718-412/debian-10-generic-amd64-daily-20190718-412.tar.xz"

    # https://github.com/NotGlop/docker-drag
    # https://devops.stackexchange.com/questions/2731/downloading-docker-images-from-docker-hub-without-using-docker
    # https://github.com/containers/skopeo

    if [[ `uname` == "FreeBSD" ]]
    then

      if ls /usr/local/bin/gpgv
      then
        echo "found gpgv"
      else
        ln -s /usr/local/bin/gpgv2 /usr/local/bin/gpgv
      fi

      debootstrap \
        --variant=minbase --exclude=systemd \
        --keyring=/usr/local/etc/debian-archive-keyring.gpg \
        --arch=amd64 "$DEBIAN_VERSION" "$MOUNTDIR" "$mirror"
      #chroot "$MOUNTDIR" /debootstrap/debootstrap --second-stage
       chroot apt-get -y update

       if chroot ${MOUNTDIR} apt-get -y install systemd systemd-sysv 
       then 
         echo "systemd install success"
       else
         echo "systemd install fail"
      fi

      if chroot ${MOUNTDIR} apt-get -y install ifupdown netbase puppet git openssh-server curl vim lsb-release python3 udev python 
       then 
         echo "other pkg install success"
       else
         echo "other pkg install fail"
      fi




    else  
      debootstrap \
        --include=systemd-sysv,ifupdown,netbase,puppet,git,openssh-server,curl,vim,lsb-release,python3,udev,python \
        "$DEBIAN_VERSION" "$MOUNTDIR" "$mirror"
    fi

  fi



  echo "${VMNAME}" > "${MOUNTDIR}/etc/hostname"

  /bin/mkdir -m 0700 "${MOUNTDIR}/root/.ssh"


  #FIXME 
  #echo "${AUTHKEY}" > "${MOUNTDIR}/root/.ssh/authorized_keys"
 
  cat /usr/local/etc/ssh_keys.txt > "${MOUNTDIR}/root/.ssh/authorized_keys"


  /bin/chmod 600 "${MOUNTDIR}/root/.ssh/authorized_keys"

  echo "/dev/vda  /     ext4  errors=remount-ro  0   1" > "${MOUNTDIR}/etc/fstab"
  #  echo "/dev/vdb  none  swap  defaults           0   0" >> "${MOUNTDIR}/etc/fstab"

  echo "source /etc/network/interfaces.d/*" > "${MOUNTDIR}/etc/network/interfaces"
  echo "" >> "${MOUNTDIR}/etc/network/interfaces"
  echo "auto lo" >> "${MOUNTDIR}/etc/network/interfaces"
  echo "iface lo inet loopback" >> "${MOUNTDIR}/etc/network/interfaces"
  echo "" >> "${MOUNTDIR}/etc/network/interfaces"
  mketh0
  echo -e "${ETH0}" >> "${MOUNTDIR}/etc/network/interfaces"
  mketh1
  echo -e "${ETH1}" >> "${MOUNTDIR}/etc/network/interfaces"

  echo "nameserver ${DNS}" > "${MOUNTDIR}/etc/resolv.conf"

  gpg --no-autostart --no-tty --homedir "${MOUNTDIR}/root/.gnupg" --import /usr/local/etc/admins.asc
  gpg --no-autostart --no-tty --homedir "${MOUNTDIR}/root/.gnupg" --import-ownertrust < /usr/local/etc/otrust.txt

  echo "puppetmaster: $PUPPETMASTER"
  #setup puppet
  if [[ $PUPPETMASTER == 'none' ]]
  then
    echo "There is no puppetmaster, using masterless"

    git clone --recursive https://git.puscii.nl/puppet1/nomasters.git  "${MOUNTDIR}/etc/puppet/code"

    echo "[Unit]" > "${MOUNTDIR}/etc/systemd/system/run-puppet.service"
    echo "Description=apply our puppet manifest" >> "${MOUNTDIR}/etc/systemd/system/run-puppet.service"
    echo "" >> "${MOUNTDIR}/etc/systemd/system/run-puppet.service"
    echo "[Service]" >> "${MOUNTDIR}/etc/systemd/system/run-puppet.service"
    echo "Type=oneshot" >> "${MOUNTDIR}/etc/systemd/system/run-puppet.service"
    echo "ExecStartPre=-/usr/bin/git -C /etc/puppet/code pull --ff-only --verify-signatures --recurse-submodules=yes" >> "${MOUNTDIR}/etc/systemd/system/run-puppet.service"
    echo "ExecStartPre=-/usr/bin/git -C /etc/puppet/code submodule update" >> "${MOUNTDIR}/etc/systemd/system/run-puppet.service"
    echo "ExecStart=/usr/bin/puppet apply /etc/puppet/code/manifests/%H.pp" >> "${MOUNTDIR}/etc/systemd/system/run-puppet.service"
    echo "User=root" >> "${MOUNTDIR}/etc/systemd/system/run-puppet.service"
    echo "Group=root" >> "${MOUNTDIR}/etc/systemd/system/run-puppet.service"

    echo "[Unit]" > "${MOUNTDIR}/etc/systemd/system/run-puppet.timer"
    echo "Description=apply our puppet manifest every hour" >> "${MOUNTDIR}/etc/systemd/system/run-puppet.timer"
    echo "" >> "${MOUNTDIR}/etc/systemd/system/run-puppet.timer"
    echo "[Timer]" >> "${MOUNTDIR}/etc/systemd/system/run-puppet.timer"
    echo "OnCalendar=*:0/30" >> "${MOUNTDIR}/etc/systemd/system/run-puppet.timer"
    echo "Persistent=true" >> "${MOUNTDIR}/etc/systemd/system/run-puppet.timer"
    echo "" >> "${MOUNTDIR}/etc/systemd/system/run-puppet.timer"
    echo "[Install]" >> "${MOUNTDIR}/etc/systemd/system/run-puppet.timer"
    echo "WantedBy=timers.target" >> "${MOUNTDIR}/etc/systemd/system/run-puppet.timer"

    ln -sL /etc/systemd/system/run-puppet.timer "${MOUNTDIR}/etc/systemd/system/timers.target.wants/"
    puppetfingerprint="masterless, no cert"
  else
    # there is a puppet master
    echo "There is a puppet master"
    hostname="$VMNAME.$DOMAIN"
    cat <<EOF > "${MOUNTDIR}/etc/puppet/puppet.conf"
[main]
ssldir = /var/lib/puppet/ssl
certname = $hostname
server = $PUPPETMASTER
[agent]
runinterval=5m
EOF

    chroot ${MOUNTDIR} systemctl enable puppet

    #generate puppet certs and store fingerprints

    # that command will fail, but still makes certs
    chroot ${MOUNTDIR} puppet agent -t || true

    puppetfingerprint=`chroot ${MOUNTDIR} puppet agent --test --fingerprint | cut -d ' ' -f 2` || true

  fi

  cat <<EOF >  "${MOUNTDIR}/lib/systemd/system/serial-getty\@.service "
[Unit]
Description=Serial Getty on %I
Documentation=man:agetty(8) man:systemd-getty-generator(8)
Documentation=http://0pointer.de/blog/projects/serial-console.html
BindsTo=dev-%i.device
After=dev-%i.device systemd-user-sessions.service plymouth-quit-wait.service getty-pre.target
After=rc-local.service

Before=getty.target
IgnoreOnIsolate=yes

Conflicts=rescue.service
Before=rescue.service

[Service]
ExecStart=-/sbin/agetty --autologin user --noclear --keep-baud 115200,38400,9600 %I $TERM
Type=idle
Restart=always
UtmpIdentifier=%I
TTYPath=/dev/%I
TTYReset=yes
TTYVHangup=yes
KillMode=process
IgnoreSIGPIPE=no
SendSIGHUP=yes

[Install]
WantedBy=getty.target
  
EOF


chroot ${MOUNTDIR} systemctl enable serial-getty@ttyS0.service

  
sshfingerprint=`ssh-keygen -l -f ${MOUNTDIR}/etc/ssh/ssh_host_ed25519_key`
sshfingerprintrsa=`ssh-keygen -l -f ${MOUNTDIR}/etc/ssh/ssh_host_rsa_key`

mkdir -p /var/cache/vminfo/$hostname/

echo "$puppetfingerprint" > "/var/cache/vminfo/$hostname/puppetfp.txt"
echo "$sshfingerprint" > "/var/cache/vminfo/$hostname/sshed25519fp.txt"
echo "$sshfingerprintrsa" > "/var/cache/vminfo/$hostname/sshrsafp.txt"


#MOTD
date=`date`
cat <<EOF > "${MOUNTDIR}/etc/motd"

 _____
/     \ This is: $hostname
\ x.x / Installed on: $date
 |_|_|  ssh ed25519 fingerprint: $sshfingerprint
        Puppet fingerprint: $puppetfingerprint

EOF

fi
