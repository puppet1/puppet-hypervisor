Libvirt / kvm / qemu setup

# What it does
- rootfs on lvm
- setup networking (bridges, firewall, vm config)
- creates root filesystem with debootstrap and clones the puppet repo
- uses https://git.puscii.nl/puppet1/puppet-buildkernel to build kernels / initramfs (needs to get integrated nicely)


# BUGS
- network config changes /etc/network/interfaces (a workaround is defining all netork interfaces in hiera as described here: https://github.com/example42/puppet-network#hiera-examples)

# Example hiera:

You can find example hiera data in: ./spec/fixtures/hieradata/*.[json|yaml]

# to test

bundle install
rspec spec

# experimental

- xen
- rootfs on p9 
